cmake_minimum_required(VERSION 2.8.3)

add_definitions(-std=c++11)

project(eden)

include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/Config.cmake)

add_subdirectory(src)

install(DIRECTORY include
        DESTINATION .
        COMPONENT devel
        FILES_MATCHING PATTERN "*.hpp" PATTERN "*.h"
)

install(FILES cmake/modules/Findeden.cmake DESTINATION ${INSTALL_MISC_DIR}/cmake/modules)
