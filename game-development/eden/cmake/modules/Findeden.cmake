message("<Findeden.cmake>")

set(FIND_EDEN_PATHS
    ${EDEN_ROOT}
    $ENV{EDEN_ROOT}
    ~/Library/Frameworks
    /Library/Frameworks
    /usr/local
    /usr
    /sw
    /opt/local
    /opt/csw
    /opt    
)

find_path(EDEN_INCLUDE_DIR eden
          PATH_SUFFIXES include
          PATHS ${FIND_EDEN_PATHS}
)

set(EDEN_FOUND TRUE)

foreach(FIND_EDEN_COMPONENT ${eden_FIND_COMPONENTS})
    string(TOLOWER ${FIND_EDEN_COMPONENT} FIND_EDEN_COMPONENT_LOWER)
    string(TOUPPER ${FIND_EDEN_COMPONENT} FIND_EDEN_COMPONENT_UPPER)
    set(FIND_EDEN_COMPONENT_NAME eden-${FIND_EDEN_COMPONENT_LOWER})

    find_library(EDEN_${FIND_EDEN_COMPONENT_UPPER}_LIBRARY
                 NAMES ${FIND_EDEN_COMPONENT_NAME}
                 PATH_SUFFIXES lib64 lib
                 PATHS ${FIND_EDEN_PATHS})
     
    if(EDEN_${FIND_EDEN_COMPONENT_UPPER}_LIBRARY)
        set(EDEN_${FIND_EDEN_COMPONENT_UPPER}_FOUND TRUE)
    else()
        set(EDEN_FOUND FALSE)
        set(EDEN_${FIND_EDEN_COMPONENT_UPPER}_FOUND FALSE)
        set(EDEN_${FIND_EDEN_COMPONENT_UPPER}_LIBRARY "")
        set(FIND_EDEN_MISSING "${FIND_EDEN_MISSING} EDEN_${FIND_EDEN_COMPONENT_UPPER}_LIBRARY")
    endif() 

    set(EDEN_LIBRARIES ${EDEN_LIBRARIES} "${EDEN_${FIND_EDEN_COMPONENT_UPPER}_LIBRARY}")
endforeach()

set(EDEN_DEPENDENCIES)
set(FIND_EDEN_DEPENDENCIES_NOTFOUND)

macro(find_eden_dependency output friendlyname)
    find_library(${output}
                 NAMES ${ARGN}
                 PATHS_SUFFIXES lib
                 PATH ${FIND_EDEN_PATHS}    
                 NO_SYSTEM_ENVIRONMENT_PATH
    )
    if(${${output}} STREQUAL "${output}-NOTFOUND")
        unset(output)
        set(FIND_EDEN_DEPENDENCIES_NOTFOUND "${FIND_SFML_DEPENDENCIES_NOTFOUND} ${friendlyname}")
    endif()
endmacro()

list(FIND eden_FIND_COMPONENTS "window" FIND_EDEN_WINDOW_COMPONENT)
if(NOT ${FIND_EDEN_WINDOW_COMPONENT} EQUAL -1)
    message("Find window dependencies")
    find_eden_dependency(SDL2main_LIBRARY "SDL2main" SDL2main)
    find_eden_dependency(SDL2_LIBRARY "SDL2" SDL2)

    set(EDEN_WINDOW_DEPENDENCIES ${SDL2main_LIBRARY} ${SDL2_LIBRARY})
    set(EDEN_DEPENDENCIES ${EDEN_WINDOW_DEPENDENCIES} ${EDEN_DEPENDENCIES})
endif()

if(EDEN_LIBRARIES AND FIND_EDEN_DEPENDENCIES_NOTFOUND)
    set(FIND_EDEN_ERROR "Eden found but some of its dependencies are missing (${FIND_EDEN_DEPENDENCIES_NOTFOUND})")
    set(EDEN_FOUND FALSE)
elseif(NOT EDEN_FOUND)
    set(FIND_EDEN_ERROR "Could NOT find Eden (missing: ${FIND_EDEN_MISSING})")
endif()

if(NOT EDEN_FOUND)
    if(EDEN_FIND_REQUIRED)
        message(FATAL_ERROR ${FIND_EDEN_ERROR})
    elseif(NOT EDEN_FIND_QUIETLY)
        message("${FIND_EDEN_ERROR}")
    endif()
endif()

if(EDEN_FOUND AND NOT EDEN_FIND_QUIETLY)
    message(STATUS "Found Eden in ${EDEN_INCLUDE_DIR}")
endif()

message("</Findeden.cmake>")
