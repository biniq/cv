#ifndef EDEN_GRAPHICS_HPP
#define EDEN_GRAPHICS_HPP

#include <eden/Graphics/Camera.hpp>
#include <eden/Graphics/Light.hpp>
#include <eden/Graphics/Mesh.hpp>
#include <eden/Graphics/Model.hpp>
#include <eden/Graphics/Shader.hpp>
#include <eden/Graphics/Texture.hpp>
#include <eden/Graphics/tiny_obj_looader.h>
#include <eden/Graphics/Transform.hpp>

#endif
