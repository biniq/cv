#ifndef EDEN_GRAPHICS_CAMERA
#define EDEN_GRAPHICS_CAMERA

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

namespace eden {
    struct Camera {
        Camera(const glm::vec3& t_position, const float& t_fov, const float& t_aspect, const float& t_z_near, const float& t_z_far) :
        projection(glm::perspective(t_fov, t_aspect, t_z_near, t_z_far)),
        position(t_position),
        forward(glm::vec3(0.0f, 0.0f, 1.0f)),
        up(glm::vec3(0.0f, 1.0f, 0.0f)) {}

        glm::mat4 get_view_projection() const {
            return this->projection * glm::lookAt(this->position, this->position + this->forward, this->up);
        }

        glm::mat4           projection;
        glm::vec3           position;
        glm::vec3           forward;
        glm::vec3           up;        
    };
}

#endif
