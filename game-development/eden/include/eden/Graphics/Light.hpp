#ifndef EDEN_GRAPHICS_LIGHT
#define EDEN_GRAPHICS_LIGHT

#include <glm/glm.hpp>

namespace eden {
    struct Light {
        glm::vec3               m_positions;
        glm::vec3               m_intensities;
    };
}

#endif
