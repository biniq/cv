#ifndef EDEN_GRAPHICS_MESH
#define EDEN_GRAPHICS_MESH

#include <eden/Graphics/tiny_obj_loader.h>

#include <GL/glew.h>
#include <glm/glm.hpp>

#include <vector>

namespace eden {
    class Mesh {
        public:
            Mesh(const tinyobj::mesh_t& t_mesh);
            void draw();
        private: 
            enum {
                POSITION_VB,
                TEXTURE_COORD_VB,
                NORMAL_VB,
                INDEX_VB,
                NUM_BUFFERS
            };

            GLuint                          m_VAO;
            GLuint                          m_VBO[NUM_BUFFERS];

            int                             m_num_indices;

            tinyobj::mesh_t                 m_mesh;

    };
}

#endif
