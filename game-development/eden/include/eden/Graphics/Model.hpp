#ifndef EDEN_GRAPHICS_MODEL
#define EDEN_GRAPHICS_MODEL

#include <eden/Graphics/Mesh.hpp>

#include <vector>

namespace eden {
    class Model {
        public:
            void load_from_OBJ(const std::string& t_filename, const std::string& t_directory_prefix = "data/models/");
            void draw();
        private:
            std::vector<eden::Mesh>             m_parts;
    };
}

#endif
