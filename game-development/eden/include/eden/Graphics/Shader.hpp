#ifndef EDEN_GRAPHICS_SHADER
#define EDEN_GRAPHICS_SHADER

#include <eden/Graphics/Transform.hpp>
#include <eden/Graphics/Camera.hpp>

#include <string>

#include <GL/glew.h>

namespace eden {
    class Shader {
        public:
            Shader(const std::string& t_filename, const std::string& t_directory_prefix = "data/shaders/", const std::string& t_vertex_prefix = ".vert", const std::string& t_fragment_prefix = ".frag");
            void bind();
            void update(const eden::Transform& t_transform, const eden::Camera& t_camera);
        private:
            std::string _load_from_file(const std::string& t_filename, const std::string& t_directory_prefix);
            GLuint _create_shader(const std::string& t_shader_source, unsigned int t_type);
            void _check_shader_error(GLuint t_shader, GLuint t_flag, bool t_is_program, const std::string& t_error_message);

            enum {
                VERTEX_SHADER,
                FRAGMENT_SHADER,
                NUM_SHADERS
            };

            enum {
                MVP_UNIFORM,
                MODEL_UNIFORM,
                NUM_UNIFORMS
            };

            GLuint              m_program;
            GLuint              m_shaders[NUM_SHADERS];
            GLuint              m_uniforms[NUM_UNIFORMS];
    };
}

#endif
