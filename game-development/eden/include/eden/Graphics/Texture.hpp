#ifndef EDEN_GRAPHICS_TEXTURE
#define EDEN_GRAPHICS_TEXTURE

#include <string>

#include <GL/glew.h>

namespace eden {
    class Texture {
        public:
            Texture(const std::string& t_filename, const std::string& t_directory_prefix = "data/textures/");
            void bind(unsigned int t_index);
           
        private:
           GLuint               m_texture; 
           GLint                m_uniform;
    };
}

#endif
