#ifndef EDEN_GRAPHICS_TRANSFORM
#define EDEN_GRAPHICS_TRANSFORM

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include <eden/Graphics/Camera.hpp>

namespace eden {
    struct Transform {
        Transform(const glm::vec3& t_position = glm::vec3(), const glm::vec3& t_rotation = glm::vec3(), const glm::vec3& t_scale = glm::vec3(1.0f, 1.0f, 1.0f)) :
        position(t_position),
        rotation(t_rotation),
        scale(t_scale) {}

        glm::mat4 get_model() const {
            glm::mat4 position_matrix = glm::translate(this->position);
            glm::mat4 scale_matrix = glm::scale(this->scale);

            glm::mat4 rotation_x = glm::rotate(this->rotation.x, glm::vec3(1.0, 0.0, 0.0));
            glm::mat4 rotation_y = glm::rotate(this->rotation.y, glm::vec3(0.0, 1.0, 0.0));
            glm::mat4 rotation_z = glm::rotate(this->rotation.z, glm::vec3(0.0, 0.0, 1.0));
            glm::mat4 rotation_matrix = rotation_x * rotation_y * rotation_z;

            return position_matrix * rotation_matrix * scale_matrix;
        }

        glm::mat4 get_mvp(const eden::Camera& t_camera) const {
            glm::mat4 view_projection = t_camera.get_view_projection();
            glm::mat4 model = this->get_model();

            return view_projection * model;
        }

        glm::vec3               position;
        glm::vec3               rotation;
        glm::vec3               scale;
    };
}

#endif
