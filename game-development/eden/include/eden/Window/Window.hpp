#ifndef EDEN_WINDOW_WINDOW
#define EDEN_WINDOW_WINDOW

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>
#include <GL/gl.h>

#include <string>

namespace eden {

    ////////////////////////////////////////////////////////////
    /// \brief Window that serves as a target for OpenGL rendering
    ///
    ////////////////////////////////////////////////////////////
    class Window {
        public:
            ////////////////////////////////////////////////////////////
            /// \brief Destructor
            ///
            /// Closes the window and frees all the resources attached to it.
            ///
            ////////////////////////////////////////////////////////////
            ~Window();

            ////////////////////////////////////////////////////////////
            /// \brief Create (or recreate) the window
            ///
            /// \param t_title              Title of the window
            /// \param t_screen_width       Width of the window
            /// \param t_screen_height      Height of the window
            ///
            ////////////////////////////////////////////////////////////
            bool create(const std::string& t_title, const int t_screen_width, const int t_screen_height);

            ////////////////////////////////////////////////////////////
            /// \brief Poll window event queue
            ///
            /// \param r_event              Reference to event object to use for popping event queue
            ///
            ////////////////////////////////////////////////////////////
            int poll_events(SDL_Event& r_event);

            ////////////////////////////////////////////////////////////
            /// \brief Clears current screen display buffers
            ///
            ////////////////////////////////////////////////////////////
            void clear();

            ////////////////////////////////////////////////////////////
            /// \brief Swaps screen display buffers
            ///
            ////////////////////////////////////////////////////////////
            void display();

            ////////////////////////////////////////////////////////////
            /// \brief Returns window quit state
            ///
            ////////////////////////////////////////////////////////////
            bool quit();

        private:
            ////////////////////////////////////////////////////////////
            /// \brief Private method to initialise OpenGL
            ///
            ////////////////////////////////////////////////////////////
            bool _init_gl();

            ////////////////////////////////////////////////////////////
            // Member data
            ////////////////////////////////////////////////////////////
            SDL_Window*                 m_window;               ///< Platform-specific implementation of the window
            SDL_GLContext               m_context;              ///< Platform-specific implementation of OpenGL context
            int                         m_screen_width;         ///< Screen width
            int                         m_screen_height;        ///< Screen height
            bool                        m_quit;                 ///< Window quit state
    };
}
#endif
