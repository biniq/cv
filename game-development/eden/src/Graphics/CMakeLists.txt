find_package(SDL2_image REQUIRED)

message("${SDL2_IMAGE_INCLUDE_DIR}")

set(GRAPHICS_SRC
    Mesh.cpp
    Model.cpp
    Shader.cpp
    Texture.cpp
    tiny_obj_loader.cc
)

add_library(eden-graphics ${GRAPHICS_SRC})

target_link_libraries(eden-graphics)

install(TARGETS eden-graphics
        RUNTIME DESTINATION bin COMPONENT bin
        LIBRARY DESTINATION lib COMPONENT bin
        ARCHIVE DESTINATION lib COMPONENT devel        
)
