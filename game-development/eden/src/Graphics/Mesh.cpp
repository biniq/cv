#include <eden/Graphics/Mesh.hpp>

#include <iostream>

namespace eden {
    Mesh::Mesh(const tinyobj::mesh_t& t_mesh) {
        this->m_num_indices = t_mesh.indices.size();

        glGenVertexArrays(1, &this->m_VAO);
        glBindVertexArray(this->m_VAO);

        glGenBuffers(NUM_BUFFERS, this->m_VBO);

        glBindBuffer(GL_ARRAY_BUFFER, this->m_VBO[POSITION_VB]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float) * t_mesh.positions.size(), t_mesh.positions.data(), GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
 
        glBindBuffer(GL_ARRAY_BUFFER, this->m_VBO[TEXTURE_COORD_VB]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float) * t_mesh.texcoords.size(), t_mesh.texcoords.data(), GL_STATIC_DRAW);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
               
        glBindBuffer(GL_ARRAY_BUFFER, this->m_VBO[NORMAL_VB]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float) * t_mesh.normals.size(), t_mesh.normals.data(), GL_STATIC_DRAW);
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->m_VBO[INDEX_VB]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * t_mesh.indices.size(), t_mesh.indices.data(), GL_STATIC_DRAW);
        glBindVertexArray(0);
    }

    void Mesh::draw() {
        glBindVertexArray(this->m_VAO);
        glDrawElementsBaseVertex(GL_TRIANGLES, this->m_num_indices, GL_UNSIGNED_INT, 0, 0);
        glBindVertexArray(0);
    }
}
