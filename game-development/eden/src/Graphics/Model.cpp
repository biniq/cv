#include <eden/Graphics/Model.hpp>

#include <eden/Graphics/tiny_obj_loader.h>

#include <iostream>

namespace eden {
    void Model::load_from_OBJ(const std::string& t_filename, const std::string& t_directory_prefix) {
        std::vector<tinyobj::shape_t> shapes;
        std::vector<tinyobj::material_t> materials;

        std::string error;
        bool ret = tinyobj::LoadObj(shapes, materials, error, (t_directory_prefix + t_filename + ".obj").c_str(), t_directory_prefix.c_str());    
        if (!error.empty()) {
            std::cerr << "ERROR: " << error << std::endl;
        } else {
            std::cout << "INFO: Model " << t_directory_prefix + t_filename << " loaded" << std::endl;
        }

        for (tinyobj::shape_t shape : shapes) {
            eden::Mesh mesh(shape.mesh);
            this->m_parts.push_back(mesh);
        }
    }

    void Model::draw() {
        for (eden::Mesh& mesh : this->m_parts) {
            mesh.draw();
        }
    }
}
