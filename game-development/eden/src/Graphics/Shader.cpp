#include <eden/Graphics/Shader.hpp>

#include <iostream>
#include <fstream>

namespace eden {
    Shader::Shader(const std::string& t_filename, const std::string& t_directory_prefix, const std::string& t_vertex_prefix, const std::string& t_fragment_prefix) {
        this->m_program = glCreateProgram();
        this->m_shaders[VERTEX_SHADER] = this->_create_shader(this->_load_from_file(t_filename + t_vertex_prefix, t_directory_prefix), GL_VERTEX_SHADER); 
        this->m_shaders[FRAGMENT_SHADER] = this->_create_shader(this->_load_from_file(t_filename + t_fragment_prefix, t_directory_prefix), GL_FRAGMENT_SHADER);

        int i = 0;
        for (auto& shader : this->m_shaders) {
            glAttachShader(this->m_program, this->m_shaders[i]);
            i++;
        }
        //glAttachShader(this->m_program, this->m_shaders[VERTEX_SHADER]);
        //glAttachShader(this->m_program, this->m_shaders[FRAGMENT_SHADER]);

        //Bind attributes
        glBindAttribLocation(this->m_program, 0, "in_position");
        glBindAttribLocation(this->m_program, 1, "in_texture_coord");
        glBindAttribLocation(this->m_program, 2, "in_normal");

        glLinkProgram(this->m_program);
        this->_check_shader_error(this->m_program, GL_LINK_STATUS, true, "Error Linking shader program");

        glValidateProgram(this->m_program);
        this->_check_shader_error(this->m_program, GL_LINK_STATUS, true, "Invalid shader program");

        //Set Uniforms
        this->m_uniforms[MVP_UNIFORM] = glGetUniformLocation(this->m_program, "u_mvp");
        this->m_uniforms[MODEL_UNIFORM] = glGetUniformLocation(this->m_program, "u_model");
    }

    void Shader::bind() {
        glUseProgram(this->m_program); 
    }

    void Shader::update(const eden::Transform& t_transform, const eden::Camera& t_camera) {
        glm::mat4 mvp = t_transform.get_mvp(t_camera);
        glm::mat4 model = t_transform.get_model(); 
        //Update uniforms
        glUniformMatrix4fv(this->m_uniforms[MVP_UNIFORM], 1, GL_FALSE, &mvp[0][0]);
        glUniformMatrix4fv(this->m_uniforms[MODEL_UNIFORM], 1, GL_FALSE, &model[0][0]);
    }

    std::string Shader::_load_from_file(const std::string &t_filename, const std::string& t_directory_prefix) {
        std::ifstream stream;
        stream.open(t_directory_prefix + t_filename);        
        std::string result;
        if (!stream.good()) {
            std::cerr << "ERROR: Could not open file: " << t_directory_prefix + t_filename << std::endl;
        } else {
            //std::cout << "INFO: Loaded file: " << t_directory_prefix + t_filename << std::endl;
            for (std::string line; std::getline(stream, line);) {
                result.append(line + "\n");
            }
        }
        stream.close();
        return result;
    }

    GLuint Shader::_create_shader(const std::string& t_shader_source, unsigned int t_type) {
        GLuint shader = glCreateShader(t_type);
        
        if (shader == 0) {
            std::cerr << "ERROR: Error compiling shader [type= " << t_type << "]" << std::endl;
        } 

        const GLchar* p[1];
        p[0] = t_shader_source.c_str();
        GLint lengths[1];
        lengths[0] = t_shader_source.length();

        glShaderSource(shader, 1, p, lengths);
        glCompileShader(shader);

        this->_check_shader_error(shader, GL_COMPILE_STATUS, false, "Error compiling shader!");
        return shader;
    }

    void Shader::_check_shader_error(GLuint t_shader, GLuint t_flag, bool t_is_program, const std::string& t_error_message) {
        GLint success = 0;
        GLchar error[1024] = {0};

        if(t_is_program) {
            glGetProgramiv(t_shader, t_flag, &success);
        } else {
            glGetShaderiv(t_shader, t_flag, &success);
        }

        if (success == GL_FALSE) {
            if (t_is_program) {
                glGetProgramInfoLog(t_shader, sizeof(error), NULL, error);
            } else {
                glGetShaderInfoLog(t_shader, sizeof(error), NULL, error);
            }
            std::cerr << "ERROR: " << t_error_message << ": " << error << std::endl;
        }
    }
}
