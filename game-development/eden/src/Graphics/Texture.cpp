#include <eden/Graphics/Texture.hpp>

#include <iostream>

#include <SDL2/SDL_image.h>

namespace eden {
    Texture::Texture(const std::string& t_filename, const std::string& t_directory_prefix) {
        SDL_Surface* texture = IMG_Load((t_directory_prefix + t_filename).c_str()); 
        if (texture == nullptr) {
            std::cerr << "ERROR: Could not load texture: " << t_directory_prefix + t_filename << std::endl;
        } else {
            glGenTextures(1, &this->m_texture);
            glBindTexture(GL_TEXTURE_2D, this->m_texture);

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture->w, texture->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture->pixels);

        }
    }

    void Texture::bind(unsigned int t_index) {
        glActiveTexture(GL_TEXTURE0 + t_index);
        glBindTexture(GL_TEXTURE_2D, this->m_texture);
    }
}
