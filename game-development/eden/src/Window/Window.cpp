#include <eden/Window/Window.hpp>

#include <iostream>

namespace eden {
    Window::~Window() {
        SDL_DestroyWindow(this->m_window);
        SDL_Quit();
    }

    bool Window::create(const std::string& t_title, const int t_screen_width, const int t_screen_height) {
        this->m_screen_width = t_screen_width;
        this->m_screen_height = t_screen_height;
        
        if (SDL_Init(SDL_INIT_VIDEO) != 0) {
            std::cerr << "SDL_Init Error: " << SDL_GetError() << std::endl;
            return false;
        }

        this->m_window = SDL_CreateWindow(t_title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, this->m_screen_width, this->m_screen_height, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

        if (this->m_window == nullptr) {
            std::cerr << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
            return false;
        }

        this->m_context = SDL_GL_CreateContext(this->m_window);
        if (this->m_context == nullptr) {
            std::cerr << "SDL_GL_CreateContext Error: " << SDL_GetError() << std::endl; 
            return false;
        } else {
            glewExperimental = GL_TRUE;
            GLenum glew_error = glewInit();
            if(glew_error != GLEW_OK) {
                std::cout << "Error initializing GLEW! " << glewGetErrorString(glew_error) << std::endl;
            }
        }
        this->_init_gl();
        return true;
    }

    int Window::poll_events(SDL_Event& r_event) {
        int result = SDL_PollEvent(&r_event);
        if(r_event.type == SDL_QUIT) {
            this->m_quit = true;
        }
        return result;
    }

    void Window::clear() {
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    void Window::display() {
        SDL_GL_SwapWindow(this->m_window);         
    }
    
    bool Window::quit() {
        return this->m_quit;
    }

    bool Window::_init_gl() {
        SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

        SDL_GL_SetSwapInterval(1);  

        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        return true; 
    }
}
