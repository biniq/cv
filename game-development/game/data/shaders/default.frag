#version 400

varying vec2 v_texture_coords;

out vec4 out_color;

uniform sampler2D u_texture;

void main () {
    //out_color = vec4(1.0, 1.0, 1.0, 1.0);
    out_color = texture(u_texture, v_texture_coords);
}
