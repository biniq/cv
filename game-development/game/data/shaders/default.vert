#version 430 core

layout(location=0) in vec3 in_position;
layout(location=1) in vec2 in_texture_coord;

varying vec2 v_texture_coords;

uniform mat4 u_mvp;
uniform mat4 u_model;

void main () {
    gl_Position = u_mvp * vec4(in_position, 1.0);
    v_texture_coords = in_texture_coord;
}
