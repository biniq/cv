#include <eden/Window/Window.hpp>
#include <eden/Graphics/Model.hpp>
#include <eden/Graphics/Texture.hpp>
#include <eden/Graphics/Shader.hpp>
#include <eden/Graphics/Transform.hpp>
#include <eden/Graphics/Camera.hpp>
#include <iostream>
#include <vector>

#include <GL/glew.h>

int main() {
    eden::Window window;
    window.create("Window Title", 640, 480);

    eden::Model model;
    model.load_from_OBJ("cube");

    eden::Transform transform;

    eden::Texture texture("cube.png");

    eden::Camera camera(glm::vec3(0.f, 0.f, -5.f), 70.f, 640/480, 0.1f, 100.f);

    int counter = 0;
    while(!window.quit()) {
        eden::Shader shader("default");
        SDL_Event event;
        while(window.poll_events(event)) {
        
        }
        transform.rotation.x = counter;
        transform.rotation.z = counter;
        window.clear();
        glClearColor(0.5, 0.3, 0.5, 1.0);
        shader.bind();
        texture.bind(0);
        shader.update(transform, camera);
        model.draw();

        window.display();
        counter++;
    }
    return 0;
}
