<?php
//Extracts and stuctures GPS data and returns as array - lat = result[0], long = result[1]
function readGPSinfoEXIF($image_full_name){
  $exif=exif_read_data($image_full_name, 0, true);
    if(!$exif || $exif['GPS']['GPSLatitude'] == '') {
      return false;
    }else{
      $lat_ref = $exif['GPS']['GPSLatitudeRef']; 
      $lat = $exif['GPS']['GPSLatitude'];
      list($num, $dec) = explode('/', $lat[0]);
      $lat_s = $num / $dec;
      list($num, $dec) = explode('/', $lat[1]);
      $lat_m = $num / $dec;
      list($num, $dec) = explode('/', $lat[2]);
      $lat_v = $num / $dec;
     
      $lon_ref = $exif['GPS']['GPSLongitudeRef'];
      $lon = $exif['GPS']['GPSLongitude'];
      list($num, $dec) = explode('/', $lon[0]);
      $lon_s = $num / $dec;
      list($num, $dec) = explode('/', $lon[1]);
      $lon_m = $num / $dec;
      list($num, $dec) = explode('/', $lon[2]);
      $lon_v = $num / $dec;
     
      $gps_int = array($lat_s + $lat_m / 60.0 + $lat_v / 3600.0, $lon_s 
                + $lon_m / 60.0 + $lon_v / 3600.0);
      return $gps_int;
    }
}

$allowedExts = array("gif", "jpeg", "jpg", "png");
$temp = explode(".", $_FILES["file"]["name"]);
$extension = end($temp);
//Upload folder must have chmod 0777 to work on UHI server
$path = "upload/";
if ((($_FILES["file"]["type"] == "image/gif")
|| ($_FILES["file"]["type"] == "image/jpeg")
|| ($_FILES["file"]["type"] == "image/jpg")
|| ($_FILES["file"]["type"] == "image/pjpeg")
|| ($_FILES["file"]["type"] == "image/x-png")
|| ($_FILES["file"]["type"] == "image/png"))
&& in_array($extension, $allowedExts)){
  if ($_FILES["file"]["error"] > 0){
    echo "Return Code: " . $_FILES["file"]["error"] . "\r\n";
  }else{
    echo "Upload: " . $_FILES["file"]["name"] . "<br>\r\n";
    echo "Type: " . $_FILES["file"]["type"] . "<br>\r\n";
    echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>\r\n";
    echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>\r\n";
    echo "\r\n<br>Metadata:<br>\r\n";
    $exif = exif_read_data($_FILES["file"]["tmp_name"]);
    var_dump($exif);
    if ($exif==false){
      echo "No header data found<br>\r\n";
    }else{
      echo "Image contains headers<br>\r\n";
      $results = readGPSinfoEXIF($_FILES["file"]["tmp_name"]);
      $latitude = $results[0];
      $longitude = $results[1];
      echo "Latitude: " . $latitude . " - Longitude: " . $longitude . "<br>\r\n";
    }   

    if (file_exists($path . $_FILES["file"]["name"])){
      echo $_FILES["file"]["name"] . " already exists. \r\n";
    }else{

      $host = "uhicomputing.net";
      $user = "10004294";
      $password = "24111993";
      $db = mysql_connect($host, $user, $password);
      if(!$db){
        die("Could not connect to " . $host);
      }else{
        mysql_select_db("TeamProject");
        move_uploaded_file($_FILES["file"]["tmp_name"], $path . $_FILES["file"]["name"]);
        echo "<br>\r\n";
        echo "Stored in: " . $path . $_FILES["file"]["name"] . "<br>\r\n";
        //TODO - Add file references and GPS data to database
        //Create page to display images and GPS data
        $name = $_FILES["file"]["name"];
        $fullpath = $path . $name;
        $query = "INSERT INTO images (filename, fullpath, lat, lng) VALUES ('$name', '$fullpath', '$latitude', '$longitude')";
        $result = mysql_query($query);
        if (!$result) {
            die('Invalid query: ' . mysql_error());
        }else{
          echo "Record Inserted";
        }
      }
    }
  }
}else{
  echo "Invalid file\r\n";
}
?>