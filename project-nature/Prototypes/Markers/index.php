<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<meta charset="utf-8">
<title>Simple markers</title>
<style>
	html, body, #map-canvas {
		height: 100%;
		margin: 0px;
		padding: 0px
	}
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDL4p7VxVNbHF14B1o8-YzPkna0PBasmSc&sensor=false&libraries=visualization"></script>
<script>
	function initialize() {
		var myLatlng = new google.maps.LatLng(56.5805009,-3.5476614);
		var mapOptions = {
			zoom: 15,
			center: myLatlng
		}
		var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		
		var locations = [
			<?php
				@ $db = mysql_connect('uhicomputing.net', '10004294', '24111993');
				if (!$db){
					die("Error: Could not connect to database.  Please try again later.");
					exit;
				}
				mysql_select_db('Amos');
				$query = "select * from images;";
				$result = mysql_query($query);
				while($row=mysql_fetch_array($result)){
					echo "['$row[title]', '$row[description]', '$row[lat]', '$row[lng]', '$row[date_taken]', '$row[time_taken]', '$row[fullpath]'],";
				}
			?>
		];
		 
		var infowindow = new google.maps.InfoWindow();
		
		var marker, i;
		
		for (i = 0; i < locations.length; i++) {  
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(locations[i][2], locations[i][3]),
				map: map
			});
		
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				var content = 	"<b>Title:</b> "+
								locations[i][0]+
								"<br><b>Desctiption:</b> "+
								locations[i][1]+
								"<br><img src='/TeamProject/"+
								locations[i][6]+
								"' width='128' height='128'><br>"+
								"<b>Date Taken:</b> "+
								locations[i][4]+
								"<br><b>Time Taken:</b> "+
								locations[i][5]; 
				
				infowindow.setContent(content);
				infowindow.open(map, marker);
			}
			})(marker, i));
		}
	}
	
	google.maps.event.addDomListener(window, 'load', initialize);

</script>
</head>
<body>
	<div id="map-canvas"></div>
</body>
</html>