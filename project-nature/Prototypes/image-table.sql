DROP TABLE IF EXISTS images;

CREATE TABLE images(
image_id INT(7) AUTO_INCREMENT,
title CHAR(24) NOT NULL,
description CHAR(36) NOT NULL,
filename CHAR(24) NOT NULL,
fullpath CHAR(36) NOT NULL,
lat DECIMAL(16,14) NOT NULL,
lng DECIMAL(16,14) NOT NULL,
date_taken DATE NOT NULL,
time_taken TIME,
main_tag CHAR(26) NOT NULL,
sub_tag CHAR(26) NOT NULL,
isval INT(1),
PRIMARY KEY (image_id)
);