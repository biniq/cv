<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Heatmaps</title>
    <style>
      html, body, #map-canvas {
	  height: 100%;
        margin: 0px;
        padding: 0px
      }
      
      #map-canvas{
	      margin-top: -4px;
 
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDL4p7VxVNbHF14B1o8-YzPkna0PBasmSc&sensor=false&libraries=visualization"></script>
       <script>
var map, pointarray, heatmap;

var taxiData = [
<?php
@ $db = mysql_connect('sql.hncomputing.net', '10001659', '14011994');
   if (!$db)
    {
      echo "Error: Could not connect to database.  Please try again later.";
      exit;
    }
   mysql_select_db('Downie');
   $query = "select * from map;";
   $result = mysql_query($query);
   while($nt=mysql_fetch_array($result))
		  {
		  echo "new google.maps.LatLng($nt[lat], $nt[lng]),";
		  }
  ?>
];

function initialize() {
  var mapOptions = {
    zoom: 15,
    center: new google.maps.LatLng(56.5805009,-3.5476614),
    mapTypeId: google.maps.MapTypeId.SATELLITE
  };

  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);

  var pointArray = new google.maps.MVCArray(taxiData);

  heatmap = new google.maps.visualization.HeatmapLayer({
    data: pointArray
  });

  heatmap.setMap(map);
}

function changeRadius(r) {
      heatmap.set('radius', r * 1);
}

function changeOpacity(o) {
      heatmap.set('opacity', o);
}

map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);

  var pointArray = new google.maps.MVCArray(taxiData);

  heatmap = new google.maps.visualization.HeatmapLayer({
    data: pointArray
heatmap.setMap(map)

}


google.maps.event.addDomListener(window, 'load', initialize);

    </script>
  </head>
  <body>
  <input type="range" id="radiusSlider" onchange="changeRadius(radiusSlider.value)"  min="1" max="40" step="1" value="14">
  <input type="range" id="opacitySlider" onchange="changeOpacity(opacitySlider.value)"  min="0" max="1" step=".01" value=".6">
  <input type="button" id="toggle" value="Toggle Map Layer" onclick="if (heatmap.getMap() == null) {heatmap.setMap(map) } else {heatmap.setMap(null)}"></input>
		
		
<div id="map-canvas"></div>
  </body>
</html>